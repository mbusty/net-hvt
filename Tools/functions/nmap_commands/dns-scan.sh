#!/bin/bash
function Google_DNS()
{
    : <<'end_Free_DNS'
    Checks if hosts can be resolved with Googles DNS server and outputs a list of IPs named Google_DNS.txt
end_Free_DNS

    DIR=~/Tools/ResolvedHosts
    clear
    if [ ! -d "$DIR" ]; then
        mkdir $DIR
    fi
    Google=8.8.8.8
    if ping -qc 4 $Google >/dev/null; then
        echo "Resolving with $Google"
        nmap -sL -iL $FileIn –dns-server 8.8.8.8 | grep 'Nmap scan report for' | cut -f 5 -d ' ' > $DIR/Google_DNS.txt
    else
        echo "Unable to reach $Google ,skipping Google DNS resolution..."
    fi
}

function Free_DNS()
{
    : <<'end_Free_DNS'
    Checks if hosts can be resolved with FreeDNS and outputs a list of IPs to a file named Public_DNS.txt
end_Free_DNS

    DIR=~/Tools/ResolvedHosts
    HOST2=37.235.1.177
    HOST1=37.235.1.177
    clear
    if [ ! -d "$DIR" ]; then
        mkdir $DIR
    fi
     if ping -qc 4 $HOST1 >/dev/null; then
        echo "Host $HOST1 is up" && nmap -sL -iL $FileIn –dns-server 37.235.1.177 | grep 'Nmap scan report for' | cut -f 5 -d ' ' > $DIR/Public_DNS.txt
    elif ping -qc 4 $HOST2 >/dev/null; then
         echo "Host $HOST2 is up" && nmap -sL -iL $FileIn –dns-server 37.235.1.174 | grep 'Nmap scan report for' | cut -f 5 -d ' ' > $DIR/Public_DNS.txt
    else
        echo "$HOST1 and $HOST2 are unreachable, skipping FreeDNS..."
    fi
}

function Local_DNS()
{
    : <<'end_Free_DNS'
    Checks if hosts can be resolved with the local DNS server and outputs a file named local_DNS.txt
end_Free_DNS

    DIR=~/Tools/ResolvedHosts
    clear
     if [ ! -d "$DIR" ]; then
        mkdir $DIR
    fi
    nmap -sL -iL $FileIn | grep 'Nmap scan report for' | cut -f 5 -d ' ' > $DIR/local_DNS.txt
}