#!/bin/bash
#Require the FileIn and FileOut Variable, Banner_Grab_Port requires the PortNum variable 

function Enum_Scan()
{
    clear
    : <<'end_Enum_Scan'
    # Enumarates live hosts quickly 
end_Enum_Scan

    nmap -Pn -n -sS -p 21-23,25,53,111,137,139,445,80,443,8443,8080,1433,3306,3389 --min-hostgroup 255 --min-rtt-timeout 0ms --max-rtt-timeout 250ms --max-retries 0 --max-scan-delay 0 -oG $FileOut -vvv --open -iL $FileIn 
}

function DC_Scan()
{
    clear
     : <<'end_DC_Scan'
     Searches for ports commonly open on Windows domain controllers 
end_DC_Scan

    nmap -Pn -n -sS -sV -O -p 53,88,389,636 --min-hostgroup 255 --min-rtt-timeout 0ms --max-rtt-timeout 250ms --max-retries 0 --max-scan-delay 0 -oG $FileOut -vvv --open -iL $FileIn
}

function Banner_Grab()
{
    clear
     : <<'end_Banner_grab'
     Banner Grab with port selection for web-servers,SQL servers, Mail servers, Antivirus and printers
end_Banner_grab
    
    nmap -sS -sV -n -Pn -p 25,80,110,143,389,390,443,636,515,995,1433,1521,3306,5432,5433,8080,8443,8192 --script banner -oG $FileOut -iL $FileIn -vv
}

function Banner_Grab_Port()
{
    clear
    : <<'end_Banner_grab_Port'
    Banner Grab on targets must use PortNum variable
end_Banner_grab_Port

    nmap -sS -sV -n -Pn -p $PortNum --script banner -oG $FileOut -iL $FileIn -vv
}

function Fire_Walk()
{
    clear
    : <<'end_Firewalk'
        Identify Network Firewals 
end_Firewalk
 
    nmap --script=firewalk --traceroute --script-args=firewalk.max-retries=1 -oG $FileOut -iL $FileIn
}

function Virtual_Hosts()
{
    clear
    : <<'end_Find_Hosts_On_IP'
Another tactic for expanding an attack surface is to find virtual hosts on an IP address that you are attempting to compromise
 (or assess). This can be done by using the hostmap-* scripts in the NSE collection. 
The hostmap-bfk.nse seems to work reasonably well providing a good starting point for your recon
 (IP to Host services do vary in accuracy). 
end_Find_Hosts_On_IP

    nmap -p 80 --script hostmap-bfk.nse -oG $FileOut -iL $FileIn
}

function DNS_Brute_Force()
{
    clear
    : <<'end_DNS_Brute_force'
    Find sub-domains with this script. 
    Detecting sub-domains associated with an organizations domain can reveal new targets when performing a security assessment. 
    The discovered hosts may be virtual web hosts on a single web server or may be distinct hosts on IP addresses spread across
    the world in different data centres.The dns-brute.nse script will find valid DNS (A) records by trying a list of common 
    sub-domains and finding those that successfully resolve.
end_DNS_Brute_force

    nmap -p 80 --script dns-brute.nse -oG $FileOut -iL $FileIn
}

function Trace_Locate()
{
    clear
    : <<'end_TraceRoute_Geolocation'
Perform a traceroute to your target IP address and have geolocation data plotted for each hop along the way. 
Makes correlating the reverse dns names of routers in your path with locations much easier.
end_TraceRoute_Geolocation

    nmap --traceroute --script traceroute-geolocation.nse -p 80 -oG $FileOut -iL $FileIn
}

function HTTP_Recon()
{
    clear
    : <<'end_HTTP_Recon'
One of the more aggressive tests, this script effectively brute forces a web server path in order to discover web applications in use.
 Attempts will be made to find valid paths on the web server that match a list of known paths for common web applications.
The standard test includes testing of over 2000 paths, meaning that the web server log 
will have over 2000 entries that are HTTP 404 not found, not a stealthy testing option! 
This is very similar to the famous Nikto web server testing tool (that performs 6000+ tests).
end_HTTP_Recon

    ## Fix this case statement to loop untill a valid choice is made. 
    read -p "Warnng HTTP Recon is an aggressive scan, it will test over 2000 paths...continue (y/n)?" choice
    case "$choice" in 
    y|Y ) echo "Continue selected" && nmap --script http-enum -oG $FileOut -iL $FileIn;;
    n|N ) echo "No selected, skipping HTTP Recon";;
    * ) echo "invalid selection, skipping HTTP Recon";;
    esac

}

function HTTP_Title()
{
    clear
    : <<'end_HTTP_Title'
It is not a difficult thing to find the Title of the web page from a web server, 
this script just makes it easier to get those title's in one set of results from a range of IP addresses.
Having the title of the page included in the Nmap scan results can provide context to a host,
that may identify the primary purpose of the web server and whether that server is a potential attack target.
end_HTTP_Title

    nmap --script http-title -sV -p 80 -oG $FileOut -iL $FileIn
}

#----Windows Only----

function SMB_OS_Id()
{
    clear
    : <<'end_SMB_OS_Discovery'
Determine operating system, computer name, netbios name and domain with the smb-os-discovery.nse script. 
An example use case could be to use this script to find all the Windows XP hosts on a large network, 
so they can be unplugged and thrown out (Windows XP is no longer supported by Microsoft).
The key advantage to using Nmap for something like this rather than a Microsoft native tool is that it 
will find all systems connected to the network not just those attached to a domain.
end_SMB_OS_Discovery

    nmap -p 445 --script smb-os-discovery -oG $FileOut -iL $FileIn
}

function SMB_Brute_Force()
{
    clear
    : <<'end_SMB_Brute_Force'
Another example of the smb series of NSE scripts is the smb-brute.nse that will 
attempt to brute force local accounts against the SMB service. While I would not classify brute forcing accounts as a recon 
function of the assessment process this script can lead to large amount of recon if we do get valid credentials as there are other
smb-* scripts that can be leveraged to retrieve all local user accounts (smb-enum-users.nse), groups (smb-enum-groups.nse), 
processes (smb-enum-processes.nse) and even execute processes remotely with the smb-psexec.nse script.
end_SMB_Brute_Force

    nmap -sV -p 445 --script smb-brute -oG $FileOut -iL $FileIn
}

