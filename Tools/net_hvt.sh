#!/bin/bash
clear
echo ' [version 1.0.0]                                                                              '
echo '  ________   _______  _________               ___  ___  ___      ___ _________                '  
echo ' |\   ___  \|\  ___ \|\___   ___\            |\  \|\  \|\  \    /  /|\___   ___\              '
echo ' \ \  \\ \  \ \   __/\|___ \  \_|____________\ \  \\\  \ \  \  /  / ||___ \  \_|              '
echo '  \ \  \\ \  \ \  \_|/__  \ \  \|\____________\ \   __  \ \  \/  / /     \ \  \               ' 
echo '   \ \  \\ \  \ \  \_|\ \  \ \  \|____________|\ \  \ \  \ \    / /       \ \  \              '
echo '    \ \__\\ \__\ \_______\  \ \__\              \ \__\ \__\ \__/ /         \ \__\             '
echo '     \|__| \|__|\|_______|   \|__|               \|__|\|__|\|__|/           \|__|             '
echo '                                                                                              '
echo '                                                                 ......TANGO DOWN             '
echo '                                                                                              '
echo '********** WARNING **********                                                                 '
echo 'This tool is a weapon, scaning networks with out permission could be considered a hostile act.'

: <<'end_description'
NET-HVT
Designed for use in cyber operations by the US Army 
By: SSG Peterson, Thomas July 2017 

NET-HVT is a modular script that automates network enumaration using Nmap, Scoper, gnmap-parser and scanreport. 
This is a proof of concept in an effort to design a process for network vulnerability analysis and testing.
NET-HVT produces a consistant, reliable and reprducible work product for baselining target networks. 

Requirements:

Variables for the input file and output file are required 

List of functions:
    Check_Requirements
    Enum_Scan
    DC_Scan
    Banner_Grab
    Banner_Grab_Port * Requires the PortNum variable
    Fire_Walk
    Virtual_Hosts
    DNS_Brute_Force
    Trace_Locate
    HTTP_Recon
    HTTP_Title
    SMB_OS_Id    * Windows Only 
    SMB_Brute_Force * Windows Only 
    Scan_Report 
    Gnmap_Parser
    Scoper
    Google_DNS
    Free_DNS
    Local_DNS

end_description

#----Required Variables----
FileIn=~/Tools/functions/scoper/input.txt
#FiloeOut=~/Tools/Outfile.txt
FileOut=~/Tools/
#----Required for banner grab----
PortNum=22

#----Sources for functions 
source ~/Tools/functions/requirements/check-requirements.sh
source ~/Tools/functions/nmap_commands/nmap_scan.sh
source ~/Tools/functions/nmap_commands/dns-scan.sh
source ~/Tools/functions/scanreport/scanreport.sh
source ~/Tools/functions/gnmap-parser/Gnmap-Parser.sh
source ~/Tools/functions/scoper/Scoper.sh


: <<'To_Do_List'
    1. Run tests for dependancies and output the result
    2.  
    3. ???
    4. Finalize nmap functions and detirmine order 
    5. Slap bitches / get money
    6. Add color 
    7. Custom Google / search sploit 
    8. Metasploit functionality 
To_Do_List



#----Test Calls, un-commnet to test 
#Scoper -i $IncludeFile
#Gnmap_Parser -g ~/Tools
#Scan_Report -f $FileOut -p 22 > ~/Tools/ScanReport.txt
